package simple.export;

import com.google.inject.Injector;
import javafx.scene.control.Alert;
import my.bak.trafic.core.event.Bus;
import my.bak.trafic.core.event.types.AlertEvent;
import my.bak.trafic.core.plugin.ExportPlugin;
import my.bak.trafic.core.plugin.exception.WrongParametersException;
import my.bak.trafic.core.plugin.transport.ExportDataBuilder;
import org.apache.logging.log4j.Logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.function.Consumer;

public class SimpleExport implements ExportPlugin {


    private Logger logger;
    private File file;
    private Bus bus;
    private BufferedWriter writer;


    @Override
    public String getName() {
        return "Simple Export plugin";
    }

    @Override
    public String getDescription() {
        return "Plugin which just print data to file";
    }

    @Override
    public void setUtility(Injector injector) {
        bus = injector.getInstance(Bus.class);
    }

    @Override
    public void init(Logger logger) {
        this.logger = logger;
        logger.info("Initialize plugin");
    }

    @Override
    public void setParameters(String parameters) throws WrongParametersException {
        if (parameters != null) {
            parameters = parameters.trim();
            file = new File(parameters);
            if (file.isDirectory()) {
                throw new WrongParametersException("Parameter cannot be path to directory");
            }
            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    logger.info("Creating file: {}",file.getAbsolutePath());
                }
            }
        }else {
            throw new WrongParametersException("Plugin need a path to file, where file should be to created");
        }
    }

    @Override
    public void start() {
        try {
            writer = Files.newBufferedWriter(file.toPath(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            logger.fatal(e);
            bus.publishEvent(new AlertEvent(Alert.AlertType.WARNING, "Export plugin", "Cannot open file for write", e));
        }
    }

    @Override
    public void exportData(ExportDataBuilder data) throws IOException {
        StringBuilder builder = new StringBuilder();
        Consumer<Object> consumer = item ->builder.append(item).append("|");
        data.getDirection().ifPresent(consumer);
        data.getPlace().ifPresent(consumer);
        data.getBegin().ifPresent(consumer);
        data.getEnd().ifPresent(consumer);
        data.getType().ifPresent(consumer);
        data.getValue().ifPresent(consumer);
        writer.write(builder.toString());
        writer.newLine();
    }





    @Override
    public void finish() {
        try {
            writer.close();
            logger.info("File writer was closed");
        } catch (IOException e) {
            logger.fatal(e);
            bus.publishEvent(new AlertEvent(Alert.AlertType.WARNING,"Plugin export","Cannot close file writer",e));
        }
    }

    @Override
    public void dispose() {
        logger.info("Dispose export plugin");
        bus = null;
        logger = null;
    }
}
